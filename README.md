#l’application permet de transformer une expression régulière en NFA et/ou DFA.

#Projet a base d'AngularJs 2 et javascript.

#Pour tester l'application :
  - cd /automate(NFA-TO-DFA)
  - npm start 
  
#Projet réaliser par  :
  - ELBOURAKKADI SOUSSI AYOUB
  - FOUKHAR JIHANE 
  
#Encadré par :
  - Mr. Y. BADDI 
  - Année universitaire 2017/2018
  
(Angular QuickStart Source)
